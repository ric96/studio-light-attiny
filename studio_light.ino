#include <TTP229.h>
#include <Adafruit_NeoPixel.h>

#define PIN 0
#define NUM_LED 12
#define SCL_PIN 3
#define SDO_PIN 2

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LED, PIN, NEO_RGB + NEO_KHZ800);

TTP229 ttp229(SCL_PIN, SDO_PIN);

int pattern = 1;
int temp;
int wait = 50;

int readButton()
{
  temp = ttp229.GetKey16();
  if(temp != 0 && temp <= 10)
  {
    pattern = temp;
    return 1;
  }
  else if(temp == 13)
  {
    if(wait > 0) wait--;
  }
  else if(temp == 16)
  {
    if(wait < 200) wait++;
  }
  return 0;
}

void setPattern()
{
    switch(pattern)
  {
  // Some example procedures showing how to display to the pixels:
    case 1: colorWipe(strip.Color(255, 0, 0), wait);
            break; // Red
    case 2: colorWipe(strip.Color(0, 255, 0), wait);
            break; // Green
    case 3: colorWipe(strip.Color(0, 0, 255), wait);
            break; // Blue
  // Send a theater pixel chase in...
    case 4: theaterChase(strip.Color(255, 255, 255), wait);
            break; // White
    case 5: theaterChase(strip.Color(255,   0,   0), wait);
            break; // Red
    case 6: theaterChase(strip.Color(0,   255,   0), wait);
            break; // Red
    case 7: theaterChase(strip.Color(  0,   0, 255), wait);
            break; // Blue

    case 8: rainbow(wait);
            break;
    case 9: rainbowCycle(wait);
            break;
    case 10: theaterChaseRainbow(wait);
            break;
    //default: pattern = 1;
  }
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
      if(readButton() == 1) break;
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
    if(readButton() == 1) break;
  }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
    if(readButton() == 1) break;
  }
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {  //do 10 cycles of chasing
    for (int q=0; q < 3; q++) {
      for (int i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip.show();
     
      delay(wait);
     
      for (int i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
    if(readButton() == 1) break;
  }
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
        for (int i=0; i < strip.numPixels(); i=i+3) {
          strip.setPixelColor(i+q, Wheel( (i+j) % 255));    //turn every third pixel on
        }
        strip.show();
       
        delay(wait);
       
        for (int i=0; i < strip.numPixels(); i=i+3) {
          strip.setPixelColor(i+q, 0);        //turn every third pixel off
        }
    }
    if(readButton() == 1) break;
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
   return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
   return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
   WheelPos -= 170;
   return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}

void setup() {
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  pinMode(5, INPUT);

}

void loop() {
  setPattern();
}
